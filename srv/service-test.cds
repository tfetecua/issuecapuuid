using { testEntity as schemaTestEntity} from '../db/issueUUID.cds';

@path : 'service/testEntity'
service TestEntityService {
  entity testEntities as projection on schemaTestEntity;
  annotate testEntities with @odata.draft.enabled; 
}