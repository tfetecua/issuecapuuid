entity testEntity {
  key ID       : UUID;
      field1   : String;
      children : Composition of many {
                   key ID     : UUID;
                       field2 : String;
                 }
}